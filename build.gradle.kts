import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
	id("org.springframework.boot") version "3.1.1"
	id("io.spring.dependency-management") version "1.1.0"
	id("com.palantir.docker") version "0.35.0"
	kotlin("jvm") version "1.8.22"
	kotlin("plugin.spring") version "1.8.22"
	idea
	groovy
}

group = "pl.mzapisek.sherlock"
version = "0.1.0"

val spockVersion = "2.3-groovy-4.0"
val groovyVersion = "4.0.10"
val testcontainersVersion = "1.19.0"

java {
	sourceCompatibility = JavaVersion.VERSION_17
}

repositories {
	mavenCentral()
}

sourceSets {
	create("integration") {
		compileClasspath += sourceSets.main.get().output
		compileClasspath += sourceSets.test.get().output
		runtimeClasspath += sourceSets.main.get().output
		runtimeClasspath += sourceSets.test.get().output
	}
}

idea.module {
	testSourceDirs = testSourceDirs + project.sourceSets["integration"].allJava.srcDirs + project.sourceSets["test"].allJava.srcDirs
}

configurations["integrationRuntimeOnly"].extendsFrom(configurations.runtimeOnly.get())

val integrationImplementation: Configuration by configurations.getting {
	extendsFrom(configurations.implementation.get())
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.springframework.boot:spring-boot-starter-validation")
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	runtimeOnly("com.mysql:mysql-connector-j:9.0.0")
	implementation("org.springframework.boot:spring-boot-starter-actuator")
	implementation("io.github.oshai:kotlin-logging-jvm:5.1.0")

	testImplementation("org.springframework.boot:spring-boot-starter-test")
	testImplementation("org.spockframework:spock-core:$spockVersion")
	testImplementation("org.apache.groovy:groovy-all:$groovyVersion")

	integrationImplementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.8")
	integrationImplementation("org.springframework.boot:spring-boot-starter-test")
	integrationImplementation("org.spockframework:spock-core:$spockVersion")
	integrationImplementation("org.spockframework:spock-spring:$spockVersion")
	integrationImplementation("org.apache.groovy:groovy-all:$groovyVersion")
	integrationImplementation("org.testcontainers:mysql:1.19.7")
	integrationImplementation("org.testcontainers:testcontainers:1.19.7")
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs += "-Xjsr305=strict"
		jvmTarget = "17"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

val integration = task<Test>("integration") {
	description = "Runs integration tests."
	group = "verification"
	testClassesDirs = sourceSets["integration"].output.classesDirs
	classpath = sourceSets["integration"].runtimeClasspath
	shouldRunAfter("test")
}

tasks.build {
	dependsOn(tasks.check)
}

tasks.check {
	dependsOn(tasks.test)
	dependsOn(integration)
}

tasks.dockerPrepare {
	dependsOn(tasks.bootJar)
}

docker {
	name = "${project.group}/${project.name}:${project.version}"
	files("./build/libs/linker-0.1.0.jar")
}