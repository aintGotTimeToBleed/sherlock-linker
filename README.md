# Sherlock Linker

## Todo
- add objects (car, person, phone number, address, claim number etc)
- links/nodes can be added manually (via rest) 
- links/nodes can be added automatically (consume msgs from topic)
- kafka msg is created during notification (on the sherlock api side)
- networks feature (generated/calculated networks are stored in dedicated location to speed up process)
- external configuration with thresholds etc
- response = sna measures, risk, number of networks

- what is better? 
  - msg is read and after computing result is sent to dedicated topic read by sherlock claims
  - msg is read from topic but sherlock claims gets the response via rest api 

## build
```
JAVA_HOME=/usr/lib/jvm/java-1.17.0-openjdk-amd64 ./gradlew clean build docker
```