package pl.mzapisek.sherlock.linker

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.ContextConfiguration
import org.testcontainers.containers.MySQLContainer
import org.testcontainers.utility.DockerImageName
import pl.mzapisek.sherlock.linker.utils.DbTestClient
import spock.lang.Shared
import spock.lang.Specification

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("integration")
@ContextConfiguration
abstract class BaseIntegrationSpec extends Specification {

    @LocalServerPort
    int appPort

    @Autowired
    TestRestTemplate restTemplate

    @Shared
    MySQLContainer<?> container = new MySQLContainer<>(DockerImageName.parse("mysql:8.0.36"))

    @Autowired
    DbTestClient dbTestClient

}
