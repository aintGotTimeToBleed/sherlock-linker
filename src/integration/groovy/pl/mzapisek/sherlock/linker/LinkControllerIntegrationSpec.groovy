package pl.mzapisek.sherlock.linker

import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import pl.mzapisek.sherlock.linker.link.Link

import static org.testcontainers.shaded.org.apache.commons.lang3.RandomUtils.nextLong

class LinkControllerIntegrationSpec extends BaseIntegrationSpec {

    // todo move to different test class
    def "should get root path message"() {
        given:
        def result = restTemplate.getForEntity("/", String.class)

        expect:
        result.statusCode == HttpStatus.OK
        result.body == "Linker API"
    }

    // clean db, sql script
    def "should get empty link list when there is no data in db"() {
        given:
        def result = restTemplate
                .exchange('/v1/links', HttpMethod.GET, null, new ParameterizedTypeReference<List<Link>>() {})

        expect:
        result.statusCode == HttpStatus.OK
        result.body.size() == 0
    }

    def "should add link"() {
        given:
        def id = 777L
        def link = new Link(nextLong(), "person", "car")

        when:
        def result = restTemplate.postForEntity('/v1/links', new HttpEntity(link), Link.class)

        then: 'result is success'
        result.statusCode == HttpStatus.CREATED
        result.body.getId() != id
        result.body.fromType == "person"
        result.body.toType == "car"

        and: 'there is a new link in the DB'
        def linkFromDb = dbTestClient.getLinkById(result.body.id)
        linkFromDb.id != id
        linkFromDb.fromType == "person"
        linkFromDb.toType == "car"

        and: "location header is set"
        result.getHeaders().containsKey("location")
        result.getHeaders().get("location").get(0) == "/v1/links/" + result.body.id
    }

    def "should return 404 when link by given id not found"() {
        given:
        def result = restTemplate
                .exchange('/v1/links/12356', HttpMethod.GET, null, new ParameterizedTypeReference<Link>() {})

        expect:
        result.statusCode == HttpStatus.NOT_FOUND
    }

    def "should get link by id"() {
        given:
        dbTestClient.insertLink([id: 123, fromType: "house", toType: "person"])

        when:
        def result = restTemplate
                .exchange('/v1/links/123', HttpMethod.GET, null, new ParameterizedTypeReference<Link>() {})

        then:
        result.statusCode == HttpStatus.OK
        result.hasBody()
    }

}
