package pl.mzapisek.sherlock.linker

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import org.springframework.http.HttpStatus
import pl.mzapisek.sherlock.linker.sna.SnaResult

class SnaControllerIntegrationSpec extends BaseIntegrationSpec {

    def "should get root path message"() {
        given:
        def mapper = new ObjectMapper()
        mapper.registerModule(new KotlinModule())
        def result = restTemplate.getForEntity("/v1/sna/claim/test", String.class)

        expect:
        SnaResult snaResult = mapper.readValue(result.getBody(), SnaResult.class)
        snaResult.getClaimId() == "test"
        snaResult.statusCode == HttpStatus.OK.value()
        snaResult.getResults().get("risk") == "no risk"
    }

}
