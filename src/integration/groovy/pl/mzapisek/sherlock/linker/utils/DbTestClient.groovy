package pl.mzapisek.sherlock.linker.utils

import org.intellij.lang.annotations.Language
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Service
import pl.mzapisek.sherlock.linker.link.Link

import java.sql.ResultSet
import java.sql.SQLException

@Service
class DbTestClient {

    @Language("MySQL")
    private static final String INSERT_LINK = """
            INSERT INTO links (id, from_type, to_type) VALUES (:id, :fromType, :toType)
    """

    @Language("MySQL")
    private static final String GET_LINK_BY_ID = """
            SELECT *
            FROM links
            WHERE id = :id
    """

    private static def LINK_ROW_MAPPER = new RowMapper() {
        @Override
        Link mapRow(ResultSet rs, int rowNum) throws SQLException {
            return new Link(
                    rs.getObject("id", Long.class),
                    rs.getString("from_type"),
                    rs.getString("to_type")
            )
        }
    }

    NamedParameterJdbcTemplate template

    DbTestClient(NamedParameterJdbcTemplate template) {
        this.template = template
    }

    void insertLink(Map args = [:]) {
        template.update(INSERT_LINK,
                new MapSqlParameterSource([
                        id      : args.id ?: 999L,
                        fromType: args.fromType ?: 'unknown',
                        toType  : args.toType ?: 'unknown'
                ])
        )
    }

    Link getLinkById(Long id) {
        return this.template.queryForObject(GET_LINK_BY_ID, ['id': id], LINK_ROW_MAPPER) as Link
    }

}
