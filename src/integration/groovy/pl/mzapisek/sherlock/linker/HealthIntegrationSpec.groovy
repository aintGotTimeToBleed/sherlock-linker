package pl.mzapisek.sherlock.linker

import org.springframework.boot.test.web.server.LocalManagementPort

class HealthIntegrationSpec extends BaseIntegrationSpec {

    @LocalManagementPort
    int mgmtPort

    def "should return 200 status for management path: health"() {
        when:
        HttpURLConnection connection = new URL("http://localhost:$mgmtPort/_/health").openConnection() as HttpURLConnection

        then:
        connection.getResponseCode() == 200
    }

}
