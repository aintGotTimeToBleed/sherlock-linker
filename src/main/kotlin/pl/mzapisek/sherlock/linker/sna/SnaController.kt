package pl.mzapisek.sherlock.linker.sna

import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RequestMapping("/v1/sna")
@RestController
class SnaController {

    private val logger = KotlinLogging.logger {}

    @GetMapping("/claim/{claimId}")
    fun analyzeClaim(@PathVariable claimId: String): SnaResult {
        logger.debug { "request for claim id: $claimId" }
        return SnaResult(claimId, HttpStatus.OK.value(), mapOf(Pair("risk", "no risk")))
    }

}

data class SnaResult(
    val claimId: String,
    val statusCode: Int,
    val results: Map<String, String>
)