package pl.mzapisek.sherlock.linker.link

import jakarta.validation.Valid
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.net.URI

@RestController
class LinkController(private val linkService: LinkService) {

    // todo move outside
    @GetMapping("/")
    fun main() = "Linker API"

    // todo add limit
    @GetMapping("/v1/links")
    fun findLinks() = linkService.findAllLinks()

    @GetMapping("/v1/links/{id}")
    fun findLink(@PathVariable id: Long): ResponseEntity<Link> {
        return linkService.getLink(id)
            .map { link -> ResponseEntity.ok().body(link) }
            .orElseGet { ResponseEntity.notFound().build() }
    }

    @PostMapping("/v1/links")
    fun addLink(@Valid @RequestBody link: Link): ResponseEntity<Link> {
        val createdLink = linkService.createLink(link)
        return ResponseEntity.created(URI.create("/v1/links/" + createdLink.id))
            .body(createdLink)
    }

}
