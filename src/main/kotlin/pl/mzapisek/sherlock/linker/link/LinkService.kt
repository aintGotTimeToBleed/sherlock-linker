package pl.mzapisek.sherlock.linker.link

import org.springframework.stereotype.Service

@Service
class LinkService(private val linkRepository: LinkRepository) {

    fun findAllLinks() = linkRepository.findAll()

    fun getLink(id: Long) = linkRepository.findById(id)

    fun createLink(link: Link) = linkRepository.save(link)

}

