package pl.mzapisek.sherlock.linker

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SherlockLinkerApplication

fun main(args: Array<String>) {
	runApplication<SherlockLinkerApplication>(*args)
}
